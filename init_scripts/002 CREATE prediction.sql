CREATE TABLE prediction (
    slot_id INTEGER NOT NULL,
    db_id SERIAL PRIMARY KEY,
    prediction_value REAL NOT NULL,
    prediction_start_timestamp TIMESTAMP NOT NULL,
    prediction_delivery_timestamp TIMESTAMP
);
