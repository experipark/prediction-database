CREATE TABLE prediction_slot(
	db_id SERIAL PRIMARY KEY,
	buurt_code VARCHAR(250) NOT NULL,
	interval_start_datetime TIMESTAMP NOT NULL,
	interval_end_datetime TIMESTAMP NOT NULL,
	is_active BOOLEAN NOT NULL
);
