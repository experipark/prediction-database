#!/bin/bash
source .env
docker build -t prediction_db:latest .
docker run --name prediction_db -e POSTGRES_PASSWORD="$POSTGRES_PASSWORD" -e POSTGRES_USER="$POSTGRES_USER" -e POSTGRES_DB="$POSTGRES_DB" -p 5432:"$DB_PORT" -v prediction_db_volume -d prediction_db
